import com.assignment3.daos.FlightDao;
import com.assignment3.entities.Assistant;
import com.assignment3.entities.Flight;
import com.assignment3.entities.Travel;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class FlightDaoTestCase extends AbstractDaoTestCase<String, Flight, FlightDao>
{

    private final static int TRAVELS_COUNT = 5;
    private final static int ASSISTANTS_COUNT = 5;

    @Test
    protected void findByDate() throws ParseException
    {
        Date before = new SimpleDateFormat("yyyy-MM-dd").parse("1996-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(before);
        calendar.add(Calendar.MONTH, 1);
        Date now = calendar.getTime();
        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();

        Flight flight = getEntity();
        flight.setDepartureTime(now);
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Flight> flights = baseDao().findByDepartureTime(
                before,
                after);
        Assertions.assertEquals(1, flights.size());
    }

    @Test
    protected void orderByDepartureTimeAsc() throws ParseException
    {
        Date now = new SimpleDateFormat("yyyy-MM-dd").parse("1990-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();

        Flight flight1 = getEntity();
        flight1.setDepartureTime(now);
        Flight flight2 = getEntity();
        flight2.setDepartureTime(after);

        baseDao().create(flight1);
        baseDao().create(flight2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Flight> flights = baseDao().orderByDepartureTime(true);
        Assertions.assertEquals(now, flights.get(0).getDepartureTime());
    }

    @Test
    protected void orderByDepartureTimeDesc() throws ParseException
    {
        Date now = new SimpleDateFormat("yyyy-MM-dd").parse("1990-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);

        Flight flight1 = getEntity();
        flight1.setDepartureTime(now);
        calendar.add(Calendar.MONTH, 1);
        flight1.setArrivalTime(calendar.getTime());

        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();
        Flight flight2 = getEntity();
        flight2.setDepartureTime(after);
        calendar.add(Calendar.MONTH, 1);
        flight2.setArrivalTime(calendar.getTime());

        baseDao().create(flight1);
        baseDao().create(flight2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Flight> flights = baseDao().orderByDepartureTime(false);
        Assertions.assertEquals(after, flights.get(0).getDepartureTime());
    }

    @Test
    protected void addTravels()
    {
        Flight flight = getEntity();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            flight.getTravels().add(Utils.createTravel());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(flight, "travels"));
        Assertions.assertEquals(TRAVELS_COUNT, flight.getTravels().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(flight, "travels"));
    }

    @Test
    protected void deleteTravels()
    {
        Flight flight = getEntity();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            flight.getTravels().add(Utils.createTravel());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(TRAVELS_COUNT, flight.getTravels().size());

        flight.getTravels().clear();
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(0, flight.getTravels().size());
    }

    @Test
    protected void addSingleTravel()
    {
        Flight flight = getEntity();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            flight.getTravels().add(Utils.createTravel());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(TRAVELS_COUNT, flight.getTravels().size());

        int previous_count = flight.getTravels().size();

        flight.getTravels().add(Utils.createTravel());
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(previous_count + 1, flight.getTravels().size());
    }

    @Test
    protected void deleteSingleTravel()
    {
        Flight flight = getEntity();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            flight.getTravels().add(Utils.createTravel());
        }

        Travel travel = Utils.createTravel();
        flight.getTravels().add(travel);
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(TRAVELS_COUNT + 1, flight.getTravels().size());

        int previous_count = flight.getTravels().size();

        flight.getTravels().remove(travel);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(previous_count - 1, flight.getTravels().size());
    }

    @Test
    protected void addAssistants()
    {
        Flight flight = getEntity();
        for (int i = 0; i < ASSISTANTS_COUNT; i++)
        {
            flight.getAssistants().add(Utils.createAssistant());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(ASSISTANTS_COUNT, flight.getAssistants().size());
    }

    @Test
    protected void deleteAssistants()
    {
        Flight flight = getEntity();
        for (int i = 0; i < ASSISTANTS_COUNT; i++)
        {
            flight.getAssistants().add(Utils.createAssistant());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(ASSISTANTS_COUNT, flight.getAssistants().size());

        flight.getAssistants().clear();
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(0, flight.getAssistants().size());
    }

    @Test
    protected void addSingleAssistant()
    {
        Flight flight = getEntity();
        for (int i = 0; i < ASSISTANTS_COUNT; i++)
        {
            flight.getAssistants().add(Utils.createAssistant());
        }
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(ASSISTANTS_COUNT, flight.getAssistants().size());

        int previous_count = flight.getAssistants().size();

        flight.getAssistants().add(Utils.createAssistant());
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(previous_count + 1, flight.getAssistants().size());
    }

    @Test
    protected void deleteSingleAssistant()
    {
        Flight flight = getEntity();
        for (int i = 0; i < ASSISTANTS_COUNT; i++)
        {
            flight.getAssistants().add(Utils.createAssistant());
        }

        Assistant assistant = Utils.createAssistant();
        flight.getAssistants().add(assistant);
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(ASSISTANTS_COUNT + 1, flight.getAssistants().size());

        int previous_count = flight.getAssistants().size();

        flight.getAssistants().remove(assistant);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertEquals(previous_count - 1, flight.getAssistants().size());
    }

    @Test
    protected void addDeparture()
    {
        Flight flight = getEntity();
        flight.setDeparture(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getDeparture());
    }

    @Test
    protected void deleteDeparture()
    {
        Flight flight = getEntity();
        flight.setDeparture(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getDeparture());

        flight.setDeparture(null);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNull(flight.getDeparture());
    }

    @Test
    protected void addArrival()
    {
        Flight flight = getEntity();
        flight.setArrival(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getArrival());
    }

    @Test
    protected void deleteArrival()
    {
        Flight flight = getEntity();
        flight.setArrival(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getArrival());

        flight.setArrival(null);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNull(flight.getArrival());
    }

    @Test
    protected void addAirplane()
    {
        Flight flight = getEntity();
        flight.setArrival(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getArrival());
    }

    @Test
    protected void deleteAirplane()
    {
        Flight flight = getEntity();
        flight.setArrival(Utils.createAirport());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getArrival());

        flight.setAirplane(null);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNull(flight.getAirplane());
    }

    @Test
    protected void addCaptain()
    {
        Flight flight = getEntity();
        flight.setCaptain(Utils.createPilot());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getCaptain());
    }

    @Test
    protected void deleteCaptain()
    {
        Flight flight = getEntity();
        flight.setCaptain(Utils.createPilot());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getCaptain());

        flight.setCaptain(null);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNull(flight.getCaptain());
    }

    @Test
    protected void addCopilot()
    {
        Flight flight = getEntity();
        flight.setCopilot(Utils.createPilot());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getCopilot());
    }

    @Test
    protected void deleteCopilot()
    {
        Flight flight = getEntity();
        flight.setCopilot(Utils.createPilot());
        baseDao().create(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNotNull(flight.getCopilot());

        flight.setCopilot(null);
        baseDao().update(flight);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        flight = baseDao().find(flight.getId());
        Assertions.assertNull(flight.getCopilot());
    }

    @Override
    protected void afterUpdate(Flight old, Flight updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getDepartureTime(), updated.getDepartureTime());
        Assertions.assertNotEquals(old.getArrivalTime(), updated.getArrivalTime());
    }

    @Override
    protected FlightDao createDao()
    {
        return new FlightDao();
    }

    @Override
    protected Flight getEntity()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Flight f = Utils.createFlight();
        f.setDepartureTime(calendar.getTime());
        calendar.add(Calendar.MONTH, 1);
        f.setArrivalTime(calendar.getTime());

        return f;
    }

    @Override
    protected Flight getUpdatedEntity(Flight instance)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(instance.getDepartureTime());
        c.add(Calendar.MINUTE, 1);

        instance.setDepartureTime(c.getTime());

        c.setTime(instance.getArrivalTime());
        c.add(Calendar.MINUTE, 1);

        instance.setArrivalTime(c.getTime());
        return instance;
    }

    @Override
    protected Flight getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Flight getInvalidInterDomainEntity()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Flight f = getEntity();
        f.setArrivalTime(calendar.getTime());
        calendar.add(Calendar.MONTH, 1);
        f.setDepartureTime(calendar.getTime());

        return f;
    }

    @Override
    protected Flight getInvalidDomainUpdatedEntity(Flight instance)
    {
        return null;
    }

    @Override
    protected Flight getInvalidInterDomainUpdatedEntity(Flight instance)
    {
        return null;
    }

    @Override
    protected Flight copyEntityInstance(Flight instance)
    {
        return Utils.clone(false, instance);
    }
}
