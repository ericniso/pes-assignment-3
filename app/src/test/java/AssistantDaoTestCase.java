import com.assignment3.daos.AssistantDao;
import com.assignment3.entities.Assistant;
import com.assignment3.entities.Flight;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class AssistantDaoTestCase extends AbstractDaoTestCase<String, Assistant, AssistantDao>
{

    private final static int FLIGHTS_COUNT = 5;

    @Test
    protected void findBySurname()
    {
        Assistant assistant = Utils.createAssistant();
        assistant.setSurname("Nisoli");
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Assistant> assistants = baseDao().findBySurname("Nisoli");
        Assertions.assertEquals(1, assistants.size());
    }

    @Test
    protected void findByBirthday() throws ParseException
    {
        Date birthday = new SimpleDateFormat("yyyy-MM-dd").parse("1987-18-01");
        Assistant assistant = Utils.createAssistant();
        assistant.setBirthday(birthday);
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Assistant> assistants = baseDao().findByBirthday(birthday);
        Assertions.assertEquals(1, assistants.size());
    }

    @Test
    protected void orderBySurnameAsc()
    {
        Assistant assistant1 = Utils.createAssistant();
        assistant1.setSurname("Z");
        Assistant assistant2 = Utils.createAssistant();
        assistant2.setSurname("A");

        baseDao().create(assistant1);
        baseDao().create(assistant2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Assistant> assistants = baseDao().orderBySurname(true);
        Assertions.assertEquals("A", assistants.get(0).getSurname());
    }

    @Test
    protected void orderBySurnameDesc()
    {
        Assistant assistant1 = Utils.createAssistant();
        assistant1.setSurname("A");
        Assistant assistant2 = Utils.createAssistant();
        assistant2.setSurname("Z");

        baseDao().create(assistant1);
        baseDao().create(assistant2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Assistant> assistants = baseDao().orderBySurname(false);
        Assertions.assertEquals("Z", assistants.get(0).getSurname());
    }

    @Test
    protected void addAssistantOf()
    {
        Assistant assistant = Utils.createAssistant();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            assistant.getAssistantOf().add(Utils.createFlight());
        }
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(assistant, "assistantOf"));
        Assertions.assertEquals(FLIGHTS_COUNT, assistant.getAssistantOf().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(assistant, "assistantOf"));
    }

    @Test
    protected void deleteAssistantOf()
    {
        Assistant assistant = Utils.createAssistant();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            assistant.getAssistantOf().add(Utils.createFlight());
        }
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, assistant.getAssistantOf().size());

        assistant.getAssistantOf().clear();
        baseDao().update(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(0, assistant.getAssistantOf().size());
    }

    @Test
    protected void addSingleAssistantOf()
    {
        Assistant assistant = Utils.createAssistant();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            assistant.getAssistantOf().add(Utils.createFlight());
        }
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, assistant.getAssistantOf().size());

        int previous_count = assistant.getAssistantOf().size();

        assistant.getAssistantOf().add(Utils.createFlight());
        baseDao().update(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(previous_count + 1, assistant.getAssistantOf().size());
    }

    @Test
    protected void deleteSingleAssistanfOf()
    {
        Assistant assistant = Utils.createAssistant();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            assistant.getAssistantOf().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        assistant.getAssistantOf().add(flight);
        baseDao().create(assistant);

        SessionHelper.getEntityManager().flush();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(FLIGHTS_COUNT + 1, assistant.getAssistantOf().size());

        int previous_count = assistant.getAssistantOf().size();

        assistant.getAssistantOf().remove(flight);
        baseDao().update(assistant);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        assistant = baseDao().find(assistant.getId());
        Assertions.assertEquals(previous_count - 1, assistant.getAssistantOf().size());
    }

    @Override
    protected void afterUpdate(Assistant old, Assistant updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getName(), updated.getName());
        Assertions.assertNotEquals(old.getSurname(), updated.getSurname());
        Assertions.assertNotEquals(old.getBirthday(), updated.getBirthday());
        Assertions.assertNotEquals(old.getLanguage(), updated.getLanguage());
    }

    @Override
    protected AssistantDao createDao()
    {
        return new AssistantDao();
    }

    @Override
    protected Assistant getEntity()
    {
        return Utils.createAssistant();
    }

    @Override
    protected Assistant getUpdatedEntity(Assistant instance)
    {
        instance.setName(String.format("%s-%s", instance.getName(), "Updated"));
        instance.setSurname(String.format("%s-%s", instance.getSurname(), "Updated"));

        Calendar c = Calendar.getInstance();
        c.setTime(instance.getBirthday());
        c.add(Calendar.MINUTE, 1);

        instance.setBirthday(c.getTime());
        instance.setLanguage(String.format("%s-%s", instance.getLanguage(), "Updated"));
        return instance;
    }

    @Override
    protected Assistant getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Assistant getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Assistant getInvalidDomainUpdatedEntity(Assistant instance)
    {
        return null;
    }

    @Override
    protected Assistant getInvalidInterDomainUpdatedEntity(Assistant instance)
    {
        return null;
    }

    @Override
    protected Assistant copyEntityInstance(Assistant instance)
    {
        return Utils.clone(false, instance);
    }
}
