import com.assignment3.daos.PilotDao;
import com.assignment3.entities.Flight;
import com.assignment3.entities.Pilot;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class PilotDaoTestCase extends AbstractDaoTestCase<String, Pilot, PilotDao>
{

    private final static int FLIGHT_COUNT = 5;

    @Test
    protected void findByGrade()
    {
        Pilot pilot = getEntity();
        pilot.setGrade("Capitano");
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Pilot> pilots = baseDao().findByGrade("Capitano");
        Assertions.assertEquals(1, pilots.size());
    }

    @Test
    protected void orderByNameAsc()
    {
        Pilot pilot1 = getEntity();
        pilot1.setGrade("Z");
        Pilot pilot2 = getEntity();
        pilot2.setGrade("A");

        baseDao().create(pilot1);
        baseDao().create(pilot2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Pilot> pilots = baseDao().orderByGrade(true);
        Assertions.assertEquals("A", pilots.get(0).getGrade());
    }

    @Test
    protected void orderByNameDesc()
    {
        Pilot pilot1 = getEntity();
        pilot1.setGrade("A");
        Pilot pilot2 = getEntity();
        pilot2.setGrade("Z");

        baseDao().create(pilot1);
        baseDao().create(pilot2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Pilot> pilots = baseDao().orderByGrade(false);
        Assertions.assertEquals("Z", pilots.get(0).getGrade());
    }

    @Test
    protected void orderByNameAscAndSurnameDesc()
    {
        Pilot pilot1 = getEntity();
        pilot1.setName("Mario");
        pilot1.setSurname("Arnoldi");
        Pilot pilot2 = getEntity();
        pilot2.setName("Mario");
        pilot2.setSurname("Rossi");

        baseDao().create(pilot1);
        baseDao().create(pilot2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Pilot> pilots = baseDao().orderByNameAndSurname(true, false);
        Assertions.assertEquals("Rossi", pilots.get(0).getSurname());
    }

    @Test
    protected void addCaptainOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCaptainOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(pilot, "captainOf"));
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCaptainOf().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(pilot, "captainOf"));
    }

    @Test
    protected void deleteCaptainOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCaptainOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCaptainOf().size());

        pilot.getCaptainOf().clear();
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(0, pilot.getCaptainOf().size());
    }

    @Test
    protected void addSingleCaptainOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCaptainOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCaptainOf().size());

        int previous_count = pilot.getCaptainOf().size();

        pilot.getCaptainOf().add(Utils.createFlight());
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(previous_count + 1, pilot.getCaptainOf().size());
    }

    @Test
    protected void deleteSingleCaptainOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCaptainOf().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        pilot.getCaptainOf().add(flight);
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT + 1, pilot.getCaptainOf().size());

        int previous_count = pilot.getCaptainOf().size();

        pilot.getCaptainOf().remove(flight);
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(previous_count - 1, pilot.getCaptainOf().size());
    }

    @Test
    protected void addCopilotOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCopilotOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(pilot, "copilotOf"));
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCopilotOf().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(pilot, "copilotOf"));
    }

    @Test
    protected void deleteCopilotOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCopilotOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCopilotOf().size());

        pilot.getCopilotOf().clear();
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(0, pilot.getCopilotOf().size());
    }

    @Test
    protected void addSingleCopilotOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCopilotOf().add(Utils.createFlight());
        }
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT, pilot.getCopilotOf().size());

        int previous_count = pilot.getCopilotOf().size();

        pilot.getCopilotOf().add(Utils.createFlight());
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(previous_count + 1, pilot.getCopilotOf().size());
    }

    @Test
    protected void deleteSingleCopilotOf()
    {
        Pilot pilot = getEntity();
        for (int i = 0; i < FLIGHT_COUNT; i++)
        {
            pilot.getCopilotOf().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        pilot.getCopilotOf().add(flight);
        baseDao().create(pilot);

        SessionHelper.getEntityManager().flush();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(FLIGHT_COUNT + 1, pilot.getCopilotOf().size());

        int previous_count = pilot.getCopilotOf().size();

        pilot.getCopilotOf().remove(flight);
        baseDao().update(pilot);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        pilot = baseDao().find(pilot.getId());
        Assertions.assertEquals(previous_count - 1, pilot.getCopilotOf().size());
    }

    @Override
    protected void afterUpdate(Pilot old, Pilot updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getName(), updated.getName());
        Assertions.assertNotEquals(old.getSurname(), updated.getSurname());
        Assertions.assertNotEquals(old.getBirthday(), updated.getBirthday());
        Assertions.assertNotEquals(old.getGrade(), updated.getGrade());
    }

    @Override
    protected PilotDao createDao()
    {
        return new PilotDao();
    }

    @Override
    protected Pilot getEntity()
    {
        try
        {
            Date major = new SimpleDateFormat("yyyy-MM-dd")
                    .parse("1996-06-27");

            Pilot pilot = Utils.createPilot();
            pilot.setBirthday(major);
            return pilot;

        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected Pilot getUpdatedEntity(Pilot instance)
    {
        instance.setName(String.format("%s-%s", instance.getName(), "Updated"));
        instance.setSurname(String.format("%s-%s", instance.getSurname(), "Updated"));

        Calendar c = Calendar.getInstance();
        c.setTime(instance.getBirthday());
        c.add(Calendar.MINUTE, 1);

        instance.setBirthday(c.getTime());
        instance.setGrade(String.format("%s-%s", instance.getGrade(), "Updated"));
        return instance;
    }

    @Override
    protected Pilot getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Pilot getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Pilot getInvalidDomainUpdatedEntity(Pilot instance)
    {
        return null;
    }

    @Override
    protected Pilot getInvalidInterDomainUpdatedEntity(Pilot instance)
    {
        return null;
    }

    @Override
    protected Pilot copyEntityInstance(Pilot instance)
    {
        return Utils.clone(false, instance);
    }
}
