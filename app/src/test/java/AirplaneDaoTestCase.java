import com.assignment3.daos.AirplaneDao;
import com.assignment3.entities.Airplane;
import com.assignment3.entities.Flight;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public final class AirplaneDaoTestCase extends AbstractDaoTestCase<String, Airplane, AirplaneDao>
{

    private final static int FLIGHTS_COUNT = 5;

    @Test
    protected void findByManufacturer()
    {
        Airplane airplane = new Airplane("My manufacturer", "Test");
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airplane> airplanes = baseDao().findByManufacturer("manu");
        Assertions.assertEquals(1, airplanes.size());
    }

    @Test
    protected void findByManufacturerAndModel()
    {
        Airplane airplane = new Airplane("Manufacturer", "Model");
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airplane> airplanes = baseDao().findByManufacturerAndModel("manu", "del");
        Assertions.assertEquals(1, airplanes.size());
    }

    @Test
    protected void orderByManufacturerAsc()
    {
        Airplane airplane1 = new Airplane("Z", "Test");
        Airplane airplane2 = new Airplane("A", "Test");

        baseDao().create(airplane1);
        baseDao().create(airplane2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airplane> airplanes = baseDao().orderByManufacturer(true);
        Assertions.assertEquals("A", airplanes.get(0).getManufacturer());
    }

    @Test
    protected void orderByManufacturerDesc()
    {
        Airplane airplane1 = new Airplane("A", "Test");
        Airplane airplane2 = new Airplane("Z", "Test");

        baseDao().create(airplane1);
        baseDao().create(airplane2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airplane> airplanes = baseDao().orderByManufacturer(false);
        Assertions.assertEquals("Z", airplanes.get(0).getManufacturer());
    }

    @Test
    protected void addFlights()
    {
        Airplane airplane = Utils.createAirplane();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airplane.getFlights().add(Utils.createFlight());
        }
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(airplane, "flights"));
        Assertions.assertEquals(FLIGHTS_COUNT, airplane.getFlights().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(airplane, "flights"));
    }

    @Test
    protected void deleteFlights()
    {
        Airplane airplane = Utils.createAirplane();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airplane.getFlights().add(Utils.createFlight());
        }
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airplane.getFlights().size());

        airplane.getFlights().clear();
        baseDao().update(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(0, airplane.getFlights().size());
    }

    @Test
    protected void addSingleFlight()
    {
        Airplane airplane = Utils.createAirplane();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airplane.getFlights().add(Utils.createFlight());
        }
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airplane.getFlights().size());

        int previous_count = airplane.getFlights().size();

        airplane.getFlights().add(Utils.createFlight());
        baseDao().update(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(previous_count + 1, airplane.getFlights().size());
    }

    @Test
    protected void deleteSingleFlight()
    {
        Airplane airplane = Utils.createAirplane();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airplane.getFlights().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        airplane.getFlights().add(flight);
        baseDao().create(airplane);

        SessionHelper.getEntityManager().flush();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(FLIGHTS_COUNT + 1, airplane.getFlights().size());

        int previous_count = airplane.getFlights().size();

        airplane.getFlights().remove(flight);
        baseDao().update(airplane);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airplane = baseDao().find(airplane.getId());
        Assertions.assertEquals(previous_count - 1, airplane.getFlights().size());
    }

    @Override
    protected void afterUpdate(Airplane old, Airplane updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getModel(), updated.getModel());
        Assertions.assertNotEquals(old.getManufacturer(), updated.getManufacturer());
    }

    @Override
    protected AirplaneDao createDao()
    {
        return new AirplaneDao();
    }

    @Override
    protected Airplane getEntity()
    {
        return Utils.createAirplane();
    }

    @Override
    protected Airplane getUpdatedEntity(Airplane instance)
    {
        instance.setModel(String.format("%s-%s", instance.getModel(), "Updated"));
        instance.setManufacturer(String.format("%s-%s", instance.getManufacturer(), "Updated"));
        return instance;
    }

    @Override
    protected Airplane getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Airplane getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Airplane getInvalidDomainUpdatedEntity(Airplane instance)
    {
        return null;
    }

    @Override
    protected Airplane getInvalidInterDomainUpdatedEntity(Airplane instance)
    {
        return null;
    }

    @Override
    protected Airplane copyEntityInstance(Airplane instance)
    {
        return Utils.clone(false, instance);
    }
}
