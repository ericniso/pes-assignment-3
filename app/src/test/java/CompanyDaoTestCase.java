import com.assignment3.daos.CompanyDao;
import com.assignment3.entities.Company;
import com.assignment3.entities.Travel;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public final class CompanyDaoTestCase extends AbstractDaoTestCase<String, Company, CompanyDao>
{

    private final static int TRAVELS_COUNT = 5;
    private final static String PSW = "PASSWORD";

    @Test
    protected void findByVAT()
    {
        Company company = Utils.createCompany();
        company.setVAT("345354354435");
        baseDao().create(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Company> companies = baseDao().findByVAT("345354354435");
        Assertions.assertEquals(1, companies.size());
    }

    @Test
    protected void orderByVATAsc()
    {
        Company company1 = Utils.createCompany();
        company1.setVAT("ZZ");
        Company company2 = Utils.createCompany();
        company2.setVAT("AA");

        baseDao().create(company1);
        baseDao().create(company2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Company> clients = baseDao().orderByVAT(true);
        Assertions.assertEquals("AA", clients.get(0).getVAT());
    }

    @Test
    protected void orderByVATDesc()
    {
        Company company1 = Utils.createCompany();
        company1.setVAT("AA");
        Company company2 = Utils.createCompany();
        company2.setVAT("ZZ");

        baseDao().create(company1);
        baseDao().create(company2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Company> clients = baseDao().orderByVAT(false);
        Assertions.assertEquals("ZZ", clients.get(0).getVAT());
    }

    @Test
    protected void addTravels()
    {
        Company company = Utils.createCompany();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            company.getTravels().add(Utils.createTravel());
        }
        baseDao().create(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(company, "travels"));
        Assertions.assertEquals(TRAVELS_COUNT, company.getTravels().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(company, "travels"));
    }

    @Test
    protected void deleteTravels()
    {
        Company company = Utils.createCompany();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            company.getTravels().add(Utils.createTravel());
        }
        baseDao().create(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(TRAVELS_COUNT, company.getTravels().size());

        company.getTravels().clear();
        baseDao().update(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(0, company.getTravels().size());
    }

    @Test
    protected void addSingleTravel()
    {
        Company company = Utils.createCompany();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            company.getTravels().add(Utils.createTravel());
        }
        baseDao().create(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(TRAVELS_COUNT, company.getTravels().size());

        int previous_count = company.getTravels().size();

        company.getTravels().add(Utils.createTravel());
        baseDao().update(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(previous_count + 1, company.getTravels().size());
    }

    @Test
    protected void deleteSingleTravel()
    {
        Company company = Utils.createCompany();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            company.getTravels().add(Utils.createTravel());
        }

        Travel travel = Utils.createTravel();
        company.getTravels().add(travel);
        baseDao().create(company);

        SessionHelper.getEntityManager().flush();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(TRAVELS_COUNT + 1, company.getTravels().size());

        int previous_count = company.getTravels().size();

        company.getTravels().remove(travel);
        baseDao().update(company);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        company = baseDao().find(company.getId());
        Assertions.assertEquals(previous_count - 1, company.getTravels().size());
    }

    @Override
    protected void afterCreate(Company instance)
    {
        super.afterCreate(instance);
        Assertions.assertNotEquals(instance.getPassword(), PSW);
        Assertions.assertTrue(instance.isPasswordMatch(PSW));
    }

    @Override
    protected void afterUpdate(Company old, Company updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getUsername(), updated.getUsername());
        Assertions.assertNotEquals(old.getPassword(), updated.getPassword());
        Assertions.assertNotEquals(old.getName(), updated.getName());
        Assertions.assertNotEquals(old.getVAT(), updated.getVAT());
    }

    @Override
    protected CompanyDao createDao()
    {
        return new CompanyDao();
    }

    @Override
    protected Company getEntity()
    {
        Company company = Utils.createCompany();
        company.setPassword(PSW);

        return company;
    }

    @Override
    protected Company getUpdatedEntity(Company instance)
    {
        instance.setUsername(String.format("%s-%s", instance.getUsername(), "Updated"));
        instance.setPassword(String.format("%s-%s", instance.getPassword(), "Updated"));
        instance.setName(String.format("%s-%s", instance.getName(), "Updated"));
        instance.setVAT(String.format("%s-%s", instance.getVAT(), "Updated"));
        return instance;
    }

    @Override
    protected Company getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Company getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Company getInvalidDomainUpdatedEntity(Company instance)
    {
        return null;
    }

    @Override
    protected Company getInvalidInterDomainUpdatedEntity(Company instance)
    {
        return null;
    }

    @Override
    protected Company copyEntityInstance(Company instance)
    {
        return Utils.clone(false, instance);
    }
}
