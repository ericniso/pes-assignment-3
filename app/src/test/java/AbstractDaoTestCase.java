import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.DomainException;
import com.assignment3.daos.InterDomainException;
import com.assignment3.entities.AbstractEntity;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.*;

import javax.persistence.PersistenceUnitUtil;
import java.io.Serializable;
import java.util.List;

/**
 * Abstract class for executing common base test cases for CRUD operations.
 *
 * @param <I> the {@link AbstractEntity} primary key type
 * @param <E> the entity type
 * @param <D> the dao class type to test
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractDaoTestCase<I extends Serializable, E extends AbstractEntity<I>, D extends AbstractDao<E>>
{
    private final static int TOTAL_ENTITIES = 5;

    private D dao;
    private PersistenceUnitUtil persistenceUtil;

    @BeforeAll
    protected void setup()
    {
        dao = createDao();
        persistenceUtil = SessionHelper.getEntityManager()
                .getEntityManagerFactory().getPersistenceUnitUtil();
    }

    @BeforeEach
    protected void begin()
    {
        dao.beginMultipleOperations();
    }

    @AfterEach
    protected void end()
    {
        dao.rollbackMultipleOperations();
    }

    @Test
    protected void create()
    {
        E domainInvalid = getInvalidDomainEntity();
        E interDomainInvalid = getInvalidInterDomainEntity();

        if (domainInvalid != null)
            Assertions.assertThrows(DomainException.class, () -> dao.create(domainInvalid));

        if (interDomainInvalid != null)
            Assertions.assertThrows(InterDomainException.class, () -> dao.create(interDomainInvalid));

        E instance = getEntity();
        beforeCreate(instance);

        instance = dao.create(getEntity());
        SessionHelper.getEntityManager().flush();

        afterCreate(instance);
    }

    @Test
    protected void read()
    {
        E instance = dao.create(getEntity());
        beforeRead(instance.getId());

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        instance = dao.find(instance.getId());
        afterRead(instance.getId(), instance);
    }

    @Test
    protected void readAll()
    {
        int previous_count = dao.findAll().size();

        for (int i = 0; i < TOTAL_ENTITIES; i++)
        {
            dao.create(getEntity());
        }

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<E> entities = dao.findAll();
        Assertions.assertEquals(TOTAL_ENTITIES + previous_count, entities.size());
    }

    @Test
    protected void count()
    {
        for (int i = 0; i < TOTAL_ENTITIES; i++)
        {
            dao.create(getEntity());
        }

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        Long c = dao.count();

        Assertions.assertTrue(c >= TOTAL_ENTITIES);
    }

    @Test
    protected void update()
    {
        E domainInvalid = getInvalidDomainUpdatedEntity(getEntity());
        E interDomainInvalid = getInvalidInterDomainUpdatedEntity(getEntity());

        if (domainInvalid != null)
            Assertions.assertThrows(DomainException.class, () -> dao.create(domainInvalid));

        if (interDomainInvalid != null)
            Assertions.assertThrows(InterDomainException.class, () -> dao.create(interDomainInvalid));

        E instance = dao.create(getEntity());
        E before = copyEntityInstance(instance);
        beforeUpdate(instance.getId(), instance);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        instance = dao.update(getUpdatedEntity(instance));

        SessionHelper.getEntityManager().flush();
        afterUpdate(before, instance);
    }

    @Test
    protected void delete()
    {
        E instance = dao.create(getEntity());
        beforeDelete(instance);
        SessionHelper.getEntityManager().flush();

        dao.delete(instance);
        SessionHelper.getEntityManager().flush();

        afterDelete(instance);
    }

    protected void beforeCreate(E instance)
    {

    }

    protected void afterCreate(E instance)
    {
        Assertions.assertNotNull(instance.getId());
    }

    protected void beforeRead(Serializable id)
    {

    }

    protected void afterRead(Serializable id, E instance)
    {
        Assertions.assertNotNull(instance);
    }

    protected void beforeUpdate(Serializable id, E instance)
    {

    }

    protected void afterUpdate(E old, E updated)
    {
        Assertions.assertEquals(old.getId(), updated.getId());
    }

    protected void beforeDelete(E instance)
    {

    }

    protected void afterDelete(E instance)
    {

    }

    protected D baseDao()
    {
        return dao;
    }

    protected PersistenceUnitUtil persistenceUnitUtil()
    {
        return persistenceUtil;
    }

    protected abstract D createDao();

    protected abstract E getEntity();

    protected abstract E getInvalidDomainEntity();

    protected abstract E getInvalidInterDomainEntity();

    protected abstract E getUpdatedEntity(E instance);

    protected abstract E getInvalidDomainUpdatedEntity(E instance);

    protected abstract E getInvalidInterDomainUpdatedEntity(E instance);

    protected abstract E copyEntityInstance(E instance);
}
