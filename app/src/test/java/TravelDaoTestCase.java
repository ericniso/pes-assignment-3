import com.assignment3.daos.TravelDao;
import com.assignment3.entities.Client;
import com.assignment3.entities.Company;
import com.assignment3.entities.Flight;
import com.assignment3.entities.Travel;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class TravelDaoTestCase extends AbstractDaoTestCase<String, Travel, TravelDao>
{

    private final static int CLIENTS_COUNT = 5;
    private final static int TRAVELS_COUNT = 5;

    @Test
    protected void findByDate() throws ParseException
    {
        Date before = new SimpleDateFormat("yyyy-MM-dd").parse("1996-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(before);
        calendar.add(Calendar.MONTH, 1);
        Date now = calendar.getTime();
        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();

        Travel travel = Utils.createTravel();
        travel.setDate(now);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Travel> travels = baseDao().findByDate(
                before,
                after);
        Assertions.assertEquals(1, travels.size());
    }

    @Test
    protected void orderByDateAsc() throws ParseException
    {
        Date now = new SimpleDateFormat("yyyy-MM-dd").parse("1990-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();

        Travel travel1 = Utils.createTravel();
        travel1.setDate(now);
        Travel travel2 = Utils.createTravel();
        travel2.setDate(after);

        baseDao().create(travel1);
        baseDao().create(travel2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Travel> travels = baseDao().orderByDate(true);
        Assertions.assertEquals(now, travels.get(0).getDate());
    }

    @Test
    protected void orderByDateDesc() throws ParseException
    {
        Date now = new SimpleDateFormat("yyyy-MM-dd").parse("3990-05-27");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, 1);
        Date after = calendar.getTime();

        Travel travel1 = Utils.createTravel();
        travel1.setDate(now);
        Travel travel2 = Utils.createTravel();
        travel2.setDate(after);

        baseDao().create(travel1);
        baseDao().create(travel2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Travel> travels = baseDao().orderByDate(false);
        Assertions.assertEquals(after, travels.get(0).getDate());
    }

    @Test
    protected void addCompany()
    {
        Travel travel = getEntity();
        Company company = Utils.createCompany();
        travel.setCompany(company);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNotNull(travel.getCompany());
        Assertions.assertEquals(travel.getCompany().getId(), company.getId());
    }

    @Test
    protected void deleteCompany()
    {
        Travel travel = getEntity();
        Company company = Utils.createCompany();
        travel.setCompany(company);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNotNull(travel.getCompany());
        Assertions.assertEquals(travel.getCompany().getId(), company.getId());

        travel.setCompany(null);
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNull(travel.getCompany());
    }

    @Test
    protected void addClients()
    {
        Travel travel = getEntity();

        for (int i = 0; i < CLIENTS_COUNT; i++)
        {
            travel.getClients().add(Utils.createClient());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(travel, "clients"));
        Assertions.assertEquals(CLIENTS_COUNT, travel.getClients().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(travel, "clients"));
    }


    @Test
    protected void deleteClients()
    {
        Travel travel = getEntity();

        for (int i = 0; i < CLIENTS_COUNT; i++)
        {
            travel.getClients().add(Utils.createClient());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(CLIENTS_COUNT, travel.getClients().size());

        travel.getClients().clear();
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(0, travel.getClients().size());
    }

    @Test
    protected void addSingleClient()
    {
        Travel travel = Utils.createTravel();
        for (int i = 0; i < CLIENTS_COUNT; i++)
        {
            travel.getClients().add(Utils.createClient());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(CLIENTS_COUNT, travel.getClients().size());

        int previous_count = travel.getClients().size();

        travel.getClients().add(Utils.createClient());
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(previous_count + 1, travel.getClients().size());
    }

    @Test
    protected void deleteSingleClient()
    {
        Travel travel = Utils.createTravel();
        for (int i = 0; i < CLIENTS_COUNT; i++)
        {
            travel.getClients().add(Utils.createClient());
        }

        Client client = Utils.createClient();
        travel.getClients().add(client);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(CLIENTS_COUNT + 1, travel.getClients().size());

        int previous_count = travel.getClients().size();

        travel.getClients().remove(client);
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(previous_count - 1, travel.getClients().size());
    }

    @Test
    protected void addRelated()
    {
        Travel travel = getEntity();

        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            travel.getRelated().add(Utils.createTravel());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(travel, "related"));
        Assertions.assertEquals(TRAVELS_COUNT, travel.getRelated().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(travel, "related"));
    }

    @Test
    protected void deleteRelated()
    {
        Travel travel = getEntity();

        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            travel.getRelated().add(Utils.createTravel());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(TRAVELS_COUNT, travel.getRelated().size());

        travel.getRelated().clear();
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(0, travel.getRelated().size());
    }

    @Test
    protected void addSingleRelated()
    {
        Travel travel = Utils.createTravel();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            travel.getRelated().add(Utils.createTravel());
        }
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(TRAVELS_COUNT, travel.getRelated().size());

        int previous_count = travel.getRelated().size();

        travel.getRelated().add(Utils.createTravel());
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(previous_count + 1, travel.getRelated().size());
    }

    @Test
    protected void deleteSingleRelated()
    {
        Travel travel = Utils.createTravel();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            travel.getRelated().add(Utils.createTravel());
        }

        Travel related = Utils.createTravel();
        travel.getRelated().add(related);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(TRAVELS_COUNT + 1, travel.getRelated().size());

        int previous_count = travel.getRelated().size();

        travel.getRelated().remove(related);
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertEquals(previous_count - 1, travel.getRelated().size());
    }

    @Test
    protected void addFlight()
    {
        Travel travel = getEntity();
        Flight flight = Utils.createFlight();
        travel.setFlight(flight);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNotNull(travel.getFlight());
        Assertions.assertEquals(travel.getFlight().getId(), flight.getId());
    }

    @Test
    protected void deleteFlight()
    {
        Travel travel = getEntity();
        Flight flight = Utils.createFlight();
        travel.setFlight(flight);
        baseDao().create(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNotNull(travel.getFlight());
        Assertions.assertEquals(travel.getFlight().getId(), flight.getId());

        travel.setFlight(null);
        baseDao().update(travel);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        travel = baseDao().find(travel.getId());
        Assertions.assertNull(travel.getFlight());
    }

    @Override
    protected void afterUpdate(Travel old, Travel updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getDate(), updated.getDate());
        Assertions.assertNotEquals(old.getStatus(), updated.getStatus());
    }

    @Override
    protected TravelDao createDao()
    {
        return new TravelDao();
    }

    @Override
    protected Travel getEntity()
    {
        return Utils.createTravel();
    }

    @Override
    protected Travel getUpdatedEntity(Travel instance)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(instance.getDate());
        c.add(Calendar.MINUTE, 1);

        instance.setDate(c.getTime());
        instance.setStatus(Travel.TravelStatus.IN_PROGRESS);
        return instance;
    }

    @Override
    protected Travel getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Travel getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Travel getInvalidDomainUpdatedEntity(Travel instance)
    {
        return null;
    }

    @Override
    protected Travel getInvalidInterDomainUpdatedEntity(Travel instance)
    {
        return null;
    }

    @Override
    protected Travel copyEntityInstance(Travel instance)
    {
        return Utils.clone(false, instance);
    }
}
