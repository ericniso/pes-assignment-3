import com.assignment3.daos.AirportDao;
import com.assignment3.entities.Airport;
import com.assignment3.entities.Flight;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public final class AirportDaoTestCase extends AbstractDaoTestCase<String, Airport, AirportDao>
{
    private final static int FLIGHTS_COUNT = 5;

    @Test
    protected void findByName()
    {
        Airport airport = Utils.createAirport();
        airport.setName("BG");
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airport> airports = baseDao().findByName("BG");
        Assertions.assertEquals(1, airports.size());
    }

    @Test
    protected void findByNameOrCity()
    {
        Airport airport = Utils.createAirport();
        airport.setCity("Bergamo");
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airport> airports = baseDao().findByNameOrCity("Non so", "Bergamo");
        Assertions.assertEquals(1, airports.size());
    }

    @Test
    protected void orderByNameAsc()
    {
        Airport airport1 = Utils.createAirport();
        airport1.setName("Z");
        Airport airport2 = Utils.createAirport();
        airport2.setName("A");

        baseDao().create(airport1);
        baseDao().create(airport2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airport> airplanes = baseDao().orderByName(true);
        Assertions.assertEquals("A", airplanes.get(0).getName());
    }

    @Test
    protected void orderByNameDesc()
    {
        Airport airport1 = Utils.createAirport();
        airport1.setName("A");
        Airport airport2 = Utils.createAirport();
        airport2.setName("Z");

        baseDao().create(airport1);
        baseDao().create(airport2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Airport> airplanes = baseDao().orderByName(false);
        Assertions.assertEquals("Z", airplanes.get(0).getName());
    }

    @Test
    protected void addDepartures()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getDepartures().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(airport, "departures"));
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getDepartures().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(airport, "departures"));
    }

    @Test
    protected void deleteDepartures()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getDepartures().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getDepartures().size());

        airport.getDepartures().clear();
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(0, airport.getDepartures().size());
    }

    @Test
    protected void addSingleDeparture()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getDepartures().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getDepartures().size());

        int previous_count = airport.getDepartures().size();

        airport.getDepartures().add(Utils.createFlight());
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(previous_count + 1, airport.getDepartures().size());
    }

    @Test
    protected void deleteSingleDeparture()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getDepartures().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        airport.getDepartures().add(flight);
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT + 1, airport.getDepartures().size());

        int previous_count = airport.getDepartures().size();

        airport.getDepartures().remove(flight);
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(previous_count - 1, airport.getDepartures().size());
    }

    @Test
    protected void addArrivals()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getArrivals().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(airport, "arrivals"));
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getArrivals().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(airport, "arrivals"));
    }

    @Test
    protected void deleteArrivals()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getArrivals().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getArrivals().size());

        airport.getArrivals().clear();
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(0, airport.getArrivals().size());
    }

    @Test
    protected void addSingleArrival()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getArrivals().add(Utils.createFlight());
        }
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT, airport.getArrivals().size());

        int previous_count = airport.getArrivals().size();

        airport.getArrivals().add(Utils.createFlight());
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(previous_count + 1, airport.getArrivals().size());
    }

    @Test
    protected void deleteSingleArrival()
    {
        Airport airport = Utils.createAirport();
        for (int i = 0; i < FLIGHTS_COUNT; i++)
        {
            airport.getArrivals().add(Utils.createFlight());
        }

        Flight flight = Utils.createFlight();
        airport.getArrivals().add(flight);
        baseDao().create(airport);

        SessionHelper.getEntityManager().flush();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(FLIGHTS_COUNT + 1, airport.getArrivals().size());

        int previous_count = airport.getArrivals().size();

        airport.getArrivals().remove(flight);
        baseDao().update(airport);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        airport = baseDao().find(airport.getId());
        Assertions.assertEquals(previous_count - 1, airport.getArrivals().size());
    }

    @Override
    protected void afterUpdate(Airport old, Airport updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getName(), updated.getName());
        Assertions.assertNotEquals(old.getCity(), updated.getCity());
        Assertions.assertNotEquals(old.getState(), updated.getState());
    }

    @Override
    protected AirportDao createDao()
    {
        return new AirportDao();
    }

    @Override
    protected Airport getEntity()
    {
        return Utils.createAirport();
    }

    @Override
    protected Airport getUpdatedEntity(Airport instance)
    {
        instance.setCity(String.format("%s-%s", instance.getCity(), "Updated"));
        instance.setName(String.format("%s-%s", instance.getName(), "Updated"));
        instance.setState(String.format("%s-%s", instance.getState(), "Updated"));
        return instance;
    }

    @Override
    protected Airport getInvalidDomainEntity()
    {
        return null;
    }

    @Override
    protected Airport getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Airport getInvalidDomainUpdatedEntity(Airport instance)
    {
        return null;
    }

    @Override
    protected Airport getInvalidInterDomainUpdatedEntity(Airport instance)
    {
        return null;
    }

    @Override
    protected Airport copyEntityInstance(Airport instance)
    {
        return Utils.clone(false, instance);
    }
}
