package com.assignment3.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

/**
 * Singleton class which provides {@link EntityManager} creation using
 * environment variables as parameters and exposes the usage of such object
 * in order to execute operations on the RDBMS
 */
public final class SessionHelper
{

    /**
     * Contains the hostname of the database machine
     */
    private final static String DB_HOST_ENV = "MYSQL_HOST";

    /**
     * Contains the database name
     */
    private final static String DB_ENV = "MYSQL_DATABASE";

    /**
     * Contains the username credential for the the database
     */
    private final static String USER_ENV = "MYSQL_USER";

    /**
     * Contains the password credential for the database for the user specified
     * by {@link SessionHelper#USER_ENV}
     */
    private final static String PASSW_ENV = "MYSQL_PASSWORD";

    /**
     * Containes the name of the schema policy used by Hibernate
     */
    private final static String SCHEMA_POLICY = "HIBERNATE_SCHEMA_POLICY";

    /**
     * Tells if db queries should be printed out on console log
     */
    private final static String SQL_DEBUG = "SQL_DEBUG";

    private static SessionHelper instance = new SessionHelper();

    private EntityManager entityManager;

    private SessionHelper()
    {
        Map<String, String> env = System.getenv();
        String url = String.format("jdbc:mysql://%s:3306/%s",
                env.get(DB_HOST_ENV), env.get(DB_ENV));

        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.connection.url", url);
        properties.put("hibernate.connection.username", env.get(USER_ENV));
        properties.put("hibernate.connection.password", env.get(PASSW_ENV));
        properties.put("hibernate.hbm2ddl.auto", env.get(SCHEMA_POLICY));
        properties.put("hibernate.show_sql", env.get(SQL_DEBUG));

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("assignment3", properties);
        entityManager = entityManagerFactory.createEntityManager();
    }

    public static void beginTransaction()
    {
        getEntityManager().getTransaction().begin();
    }

    public static void commitTransaction()
    {
        getEntityManager().getTransaction().commit();
    }

    public static void rollbackTransaction()
    {
        getEntityManager().getTransaction().rollback();
    }

    private static SessionHelper getInstance()
    {
        return instance;
    }

    public static EntityManager getEntityManager()
    {
        if (!getInstance().entityManager.isOpen())
        {
            instance = new SessionHelper();
        }

        return getInstance().entityManager;
    }

    public static void closeEntityManager()
    {
        if (getInstance().entityManager.isOpen())
        {
            getInstance().entityManager.close();
        }
    }

    public static void openEntityManager()
    {
        if (!getInstance().entityManager.isOpen())
        {
            instance = new SessionHelper();
        }
    }
}
