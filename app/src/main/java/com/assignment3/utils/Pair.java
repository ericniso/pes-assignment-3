package com.assignment3.utils;

/**
 * Class which represents a pair of values with different types
 * @param <F> first value type
 * @param <S> second value type
 */
public class Pair<F, S>
{
    F first;
    S second;

    public static <F, S> Pair<F, S> from(F first, S second)
    {
        return new Pair<>(first, second);
    }

    public Pair(F first, S second)
    {
        this.first = first;
        this.second = second;
    }

    public F getFirst()
    {
        return first;
    }

    public void setFirst(F first)
    {
        this.first = first;
    }

    public S getSecond()
    {
        return second;
    }

    public void setSecond(S second)
    {
        this.second = second;
    }
}
