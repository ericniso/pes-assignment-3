package com.assignment3.daos;

import com.assignment3.entities.Travel;
import com.assignment3.utils.Pair;

import java.util.Date;
import java.util.List;

public class TravelDao extends AbstractDao<Travel>
{

    public List<Travel> findByDate(Date before, Date after)
    {
        return findByBetween("date", before, after);
    }

    public List<Travel> orderByDate(boolean ascending)
    {
        return orderBy("date", ascending);
    }

    @Override
    protected void updateRelationships(Travel instance)
    {

    }

    @Override
    protected Pair<Boolean, String> checkDomain(Travel instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Travel instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Travel> getGenericClassName()
    {
        return Travel.class;
    }
}
