package com.assignment3.daos;

import com.assignment3.entities.Flight;
import com.assignment3.entities.Travel;
import com.assignment3.utils.Pair;

import java.util.Date;
import java.util.List;

public class FlightDao extends AbstractDao<Flight>
{

    public List<Flight> findByDepartureTime(Date before, Date after)
    {
        return findByBetween("departureTime", before, after);
    }

    public List<Flight> orderByDepartureTime(boolean ascending)
    {
        return orderBy("departureTime", ascending);
    }

    @Override
    protected void updateRelationships(Flight instance)
    {
        for (Travel t : instance.getTravels())
        {
            t.setFlight(instance);
        }
    }

    @Override
    protected Pair<Boolean, String> checkDomain(Flight instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Flight instance)
    {
        if (instance.getDepartureTime().after(instance.getArrivalTime()))
            return Pair.from(false, "Departure time can't be after arrival time");

        return Pair.from(true, null);
    }

    @Override
    protected Class<Flight> getGenericClassName()
    {
        return Flight.class;
    }
}
