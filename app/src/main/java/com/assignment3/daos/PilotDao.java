package com.assignment3.daos;

import com.assignment3.entities.Flight;
import com.assignment3.entities.Pilot;
import com.assignment3.utils.Pair;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PilotDao extends AbstractDao<Pilot>
{
    public List<Pilot> findByGrade(String grade)
    {
        return findByLike("grade", grade);
    }

    public List<Pilot> orderByGrade(boolean ascending)
    {
        return orderBy("grade", ascending);
    }

    public List<Pilot> orderByNameAndSurname(boolean nameAscending, boolean surnameAscending)
    {
        return orderByMultiple(
                Pair.from("name", nameAscending),
                Pair.from("surname", surnameAscending));
    }

    @Override
    protected void updateRelationships(Pilot instance)
    {
        for (Flight f : instance.getCaptainOf())
        {
            f.setCaptain(instance);
        }

        for (Flight f : instance.getCopilotOf())
        {
            f.setCopilot(instance);
        }
    }

    @Override
    protected Pair<Boolean, String> checkDomain(Pilot instance)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(instance.getBirthday());
        calendar.add(Calendar.YEAR, 19);

        if (calendar.getTime().after(new Date()))
            return Pair.from(false, "Pilot must have at least 18 years");

        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Pilot instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Pilot> getGenericClassName()
    {
        return Pilot.class;
    }
}
