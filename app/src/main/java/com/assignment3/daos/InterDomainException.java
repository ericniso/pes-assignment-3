package com.assignment3.daos;

import com.assignment3.entities.AbstractEntity;

public class InterDomainException extends EntityException
{
    public InterDomainException(AbstractEntity entity, String message)
    {
        super(message, entity);
    }
}
