package com.assignment3.daos;

import com.assignment3.entities.AbstractEntity;

public class DomainException extends EntityException
{
    public DomainException(AbstractEntity entity, String message)
    {
        super(message, entity);
    }
}
