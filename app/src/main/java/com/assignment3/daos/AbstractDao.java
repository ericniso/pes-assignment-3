package com.assignment3.daos;


import com.assignment3.entities.AbstractEntity;
import com.assignment3.utils.Pair;
import com.assignment3.utils.SessionHelper;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class needed to provide basic CRUD operations on
 * {@link AbstractEntity} specified by the generic type {@code <E>} using the
 * {@link SessionHelper} wrapper class
 * @param <E> the {@link AbstractEntity} type
 */
public abstract class AbstractDao<E extends AbstractEntity>
{
    private Class<E> className;
    private boolean atomicOperation;

    protected AbstractDao()
    {
        this.className = getGenericClassName();
        this.atomicOperation = true;
    }

    public E create(E instance) throws DomainException, InterDomainException
    {
        updateRelationships(instance);
        check(instance);

        if (!this.atomicOperation)
        {
            SessionHelper.getEntityManager().persist(instance);
            return instance;
        }

        try
        {
            SessionHelper.beginTransaction();
            SessionHelper.getEntityManager().persist(instance);
            SessionHelper.commitTransaction();
        } catch (Exception ex)
        {
            ex.printStackTrace();
            instance = null;
            SessionHelper.rollbackTransaction();
        }

        return instance;
    }

    public E find(Serializable id)
    {
        return SessionHelper.getEntityManager().find(className, id);
    }

    public List<E> findAll()
    {
        return SessionHelper.getEntityManager().createQuery(
                createQueryPreparation().query).getResultList();
    }

    /**
     * Finder method involving {@link String} search using {@code LIKE} RDBMS
     * paradigm on a single attribute of {@code <E>}
     * @param field the name of the {@code <E>} attribute
     * @param value the value used to filter the {@code <E>} entities
     * @return a list of filtered {@code <E>}
     */
    protected List<E> findByLike(String field, String value)
    {
        QueryPreparation<E, E> qp = createQueryPreparation();
        qp.query.select(qp.root).where(
                qp.builder.like(qp.root.get(field), String.format("%%%s%%", value)));

        return SessionHelper.getEntityManager().createQuery(qp.query).getResultList();
    }

    /**
     * Finder method using exact search on {@code <E>} entity attribute
     * @param field the name of the {@code <E>} attribute
     * @param value the value used to filter the {@code <E>} entities
     * @param <T> the type of the value to search
     * @return a list of filtered {@code <E>}
     */
    protected <T> List<E> findByEqual(String field, T value)
    {
        QueryPreparation<E, E> qp = createQueryPreparation();

        return findByMultipleExp(QueryConcatType.AND, qp, qp.builder.equal(qp.root.<T>get(field), value));
    }

    /**
     * Finder method using exact search on {@code <E>} entity attribute
     * @param concatType the {@link QueryConcatType} used to link together the
     *                   {@link Predicate} provided
     * @param qp the general query object used to build the final query using
     *           the provided {@link Predicate} list
     * @param predicates the list of filters to apply to the query
     * @return a list of filtered {@code <E>}
     */
    protected List<E> findByMultipleExp(QueryConcatType concatType, QueryPreparation<E, E> qp, Predicate... predicates)
    {
        qp.query.select(qp.root);

        if (concatType.equals(QueryConcatType.AND))
        {
            qp.query.where(qp.builder.and(predicates));
        } else if (concatType.equals(QueryConcatType.OR))
        {
            qp.query.where(qp.builder.or(predicates));
        }

        TypedQuery<E> typedQuery = SessionHelper.getEntityManager().createQuery(qp.query);

        return typedQuery.getResultList();
    }

    /**
     * Finder method using search between upper/lower bounds on {@code <E>} attribute
     * @param field the name of the {@code <E>} attribute
     * @param lower the lower bound value
     * @param upper the upper bound value
     * @param <T> the type of the value to search
     * @return
     */
    protected <T extends Comparable> List<E> findByBetween(String field, T lower, T upper)
    {
        QueryPreparation<E, E> qp = createQueryPreparation();
        qp.query.select(qp.root).where(qp.builder.and(
                qp.builder.greaterThanOrEqualTo(qp.root.<T>get(field), lower),
                qp.builder.lessThanOrEqualTo(qp.root.<T>get(field), upper)));

        return SessionHelper.getEntityManager().createQuery(qp.query).getResultList();
    }

    protected List<E> orderBy(String field, boolean ascending)
    {
        return orderByMultiple(Pair.from(field, ascending));
    }

    protected List<E> orderByMultiple(Pair<String, Boolean>... pairs)
    {
        QueryPreparation<E, E> qp = createQueryPreparation();
        qp.query.select(qp.root);

        List<Order> orderList = new ArrayList<>();

        for (Pair<String, Boolean> p : pairs)
        {
            if (p.getSecond())
            {
                orderList.add(qp.builder.asc(qp.root.get(p.getFirst())));
            } else
            {
                orderList.add(qp.builder.desc(qp.root.get(p.getFirst())));
            }
        }

        qp.query.orderBy(orderList);

        return SessionHelper.getEntityManager().createQuery(qp.query).getResultList();
    }

    public Long count()
    {
        QueryPreparation<Long, E> qp = createQueryPreparation(Long.class, className);
        qp.query.select(qp.builder.count(qp.root));

        return SessionHelper.getEntityManager().createQuery(qp.query).getSingleResult();
    }

    public E update(E instance) throws DomainException, InterDomainException
    {
        updateRelationships(instance);
        check(instance);

        if (!this.atomicOperation)
        {
            return SessionHelper.getEntityManager().merge(instance);
        }

        E updatedModel;
        try
        {
            SessionHelper.beginTransaction();
            updatedModel = SessionHelper.getEntityManager().merge(instance);
            SessionHelper.commitTransaction();
        } catch (Exception ex)
        {
            ex.printStackTrace();
            SessionHelper.rollbackTransaction();
            updatedModel = null;
        }

        return updatedModel;
    }

    public void delete(E instance)
    {
        if (!this.atomicOperation)
        {
            SessionHelper.getEntityManager().remove(instance);
        } else
        {
            SessionHelper.beginTransaction();
            SessionHelper.getEntityManager().remove(instance);
            SessionHelper.commitTransaction();
        }
    }

    /**
     * Disables transactions on other CRUD methods and begins a single transaction
     * block
     */
    public void beginMultipleOperations()
    {
        this.atomicOperation = false;
        SessionHelper.beginTransaction();
    }

    /**
     * Commits previous transaction block and re-enable atomic transactions for
     * every CRUD method
     */
    public void commitMultipleOperations()
    {
        SessionHelper.commitTransaction();
        this.atomicOperation = true;
    }

    /**
     * Executes a rollback operation on current transaction and re-eanble
     * atomic transactions for every CRUD method
     */
    public void rollbackMultipleOperations()
    {
        SessionHelper.rollbackTransaction();
        this.atomicOperation = true;
    }

    /**
     * Creates a basic query object using {@code <E>} type as result type and
     * table reference
     * @return a query object
     */
    protected QueryPreparation<E, E> createQueryPreparation()
    {
        return new QueryPreparation<>(className, className);
    }

    protected <F> QueryPreparation<E, F> createQueryPreparation(Class<F> from)
    {
        return new QueryPreparation<>(className, from);
    }

    protected <F, G> QueryPreparation<F, G> createQueryPreparation(Class<F> entity, Class<G> from)
    {
        return new QueryPreparation<>(entity, from);
    }

    /**
     * Abstract method called when creating/updating the entity, useful for
     * creating the relationship in {@link javax.persistence.OneToMany}
     * attributes
     * @param instance the {@code <E>} entity instance to update
     */
    protected abstract void updateRelationships(E instance);

    protected abstract Pair<Boolean, String> checkDomain(E instance);

    protected abstract Pair<Boolean, String> checkInterDomain(E instance);

    protected abstract Class<E> getGenericClassName();

    private void check(E instance) throws DomainException, InterDomainException
    {
        Pair<Boolean, String> domain = checkDomain(instance);
        Pair<Boolean, String> interDomain = checkInterDomain(instance);

        if (!domain.getFirst())
            throw new DomainException(instance, domain.getSecond());

        if (!interDomain.getFirst())
            throw new InterDomainException(instance, interDomain.getSecond());
    }

    public final static class QueryPreparation<E, F>
    {
        CriteriaBuilder builder;
        CriteriaQuery<E> query;
        Root<F> root;

        public QueryPreparation(Class<E> entity, Class<F> from)
        {
            this.builder = SessionHelper.getEntityManager().getCriteriaBuilder();
            this.query = builder.createQuery(entity);
            this.root = this.query.from(from);
        }
    }

    protected enum QueryConcatType
    {

        AND,
        OR
    }
}

