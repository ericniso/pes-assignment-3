package com.assignment3.daos;

import com.assignment3.entities.Assistant;
import com.assignment3.utils.Pair;

import java.util.Date;
import java.util.List;

public class AssistantDao extends AbstractDao<Assistant>
{

    public List<Assistant> findBySurname(String surname)
    {
        return findByLike("surname", surname);
    }

    public List<Assistant> findByBirthday(Date birthday)
    {
        return findByEqual("birthday", birthday);
    }

    public List<Assistant> orderBySurname(boolean ascending)
    {
        return orderBy("surname", ascending);
    }

    @Override
    protected void updateRelationships(Assistant instance)
    {

    }

    @Override
    protected Pair<Boolean, String> checkDomain(Assistant instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Assistant instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Assistant> getGenericClassName()
    {
        return Assistant.class;
    }
}
