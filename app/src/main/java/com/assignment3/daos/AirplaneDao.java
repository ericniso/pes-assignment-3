package com.assignment3.daos;

import com.assignment3.entities.Airplane;
import com.assignment3.entities.Flight;
import com.assignment3.utils.Pair;

import java.util.List;

public class AirplaneDao extends AbstractDao<Airplane>
{

    public List<Airplane> findByManufacturer(String manufacturer)
    {
        return findByLike("manufacturer", manufacturer);
    }

    public List<Airplane> findByManufacturerAndModel(String manufacturer, String model)
    {
        QueryPreparation<Airplane, Airplane> qp = createQueryPreparation();

        return findByMultipleExp(QueryConcatType.AND, qp,
                qp.builder.like(qp.root.get("manufacturer"), String.format("%%%s%%", manufacturer)),
                qp.builder.like(qp.root.get("model"), String.format("%%%s%%", model)));
    }

    public List<Airplane> orderByManufacturer(boolean ascending)
    {
        return orderBy("manufacturer", ascending);
    }

    @Override
    protected void updateRelationships(Airplane instance)
    {
        for (Flight f : instance.getFlights())
        {
            f.setAirplane(instance);
        }
    }

    @Override
    protected Pair<Boolean, String> checkDomain(Airplane instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Airplane instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Airplane> getGenericClassName()
    {
        return Airplane.class;
    }
}
