package com.assignment3.daos;

import com.assignment3.entities.AbstractEntity;

public abstract class EntityException extends RuntimeException
{
    private AbstractEntity entity;

    public EntityException(String message, AbstractEntity entity)
    {
        super(message);
        this.entity = entity;
    }

    public <T extends AbstractEntity> AbstractEntity getEntity()
    {
        return (T) entity;
    }
}
