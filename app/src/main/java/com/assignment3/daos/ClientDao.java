package com.assignment3.daos;

import com.assignment3.entities.Client;
import com.assignment3.utils.Pair;

import java.util.Date;
import java.util.List;

public class ClientDao extends AbstractDao<Client>
{
    public List<Client> findByUsername(String username)
    {
        return findByLike("username", username);
    }

    public List<Client> orderByUsername(boolean ascending)
    {
        return orderBy("username", ascending);
    }

    @Override
    protected void updateRelationships(Client instance)
    {

    }

    @Override
    protected Pair<Boolean, String> checkDomain(Client instance)
    {
        if (instance.getBirthday().after(new Date()))
            return Pair.from(false, "Client can't be born after today");

        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Client instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Client> getGenericClassName()
    {
        return Client.class;
    }
}
