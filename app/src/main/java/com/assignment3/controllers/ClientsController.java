package com.assignment3.controllers;

import com.assignment3.entities.Client;
import com.assignment3.proxies.ClientProxy;
import com.assignment3.services.ClientService;
import com.assignment3.services.AbstractService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClientsController extends ControllerBase<Client, ClientProxy>
{
    @Override
    protected AbstractService<Client> createService()
    {
        return new ClientService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Client getEmptyModel()
    {
        return new Client();
    }

    @Override
    protected ClientProxy createProxy(Client instance)
    {
        return new ClientProxy(instance);
    }

    @Override
    protected List<ClientProxy> createProxy(List<Client> instance)
    {
        List<ClientProxy> list = new ArrayList<>();

        for (Client i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
