package com.assignment3.controllers;

import com.assignment3.entities.AbstractEntity;
import com.assignment3.proxies.AbstractProxy;
import com.assignment3.services.AbstractService;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.apache.struts2.rest.RestActionSupport;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract class which provides common CRUD methods for {@link AbstractEntity}
 * instances. It uses method names for REST handlers as shown in the plugin
 * guide <a href="https://struts.apache.org/plugins/rest/">REST plugin</a>.
 * It uses a {@code P} instance to handle request and response json objects.
 * @param <E> the {@link AbstractEntity} type
 * @param <P> the {@link AbstractProxy} type
 */
public abstract class ControllerBase<E extends AbstractEntity, P extends AbstractProxy<E>>
        extends RestActionSupport implements ModelDriven<Object>
{
    private final static String DEFAULT_ERR_MSG = "Unexpected error";

    private String id;
    private P model;
    private List<P> list;
    private AbstractService<E> service;
    private Error error;

    protected ControllerBase()
    {
        service = createService();
        model = createProxy(getEmptyModel());
    }

    public HttpHeaders index()
    {
        list = createProxy(service.findAll());
        return new DefaultHttpHeaders("index");
    }

    public HttpHeaders show()
    {
        HttpHeaders result = new DefaultHttpHeaders("show");

        try
        {
            E res = service.find(convertId(id));

            if (service.hasPendingError())
            {
                error = new Error(service.getPendingErrorMessage());
                result.setStatus(404);
            } else if (res == null)
            {
                error = new Error(DEFAULT_ERR_MSG);
                result.setStatus(400);
            } else
            {
                model = createProxy(res);
            }
        } catch (Exception e)
        {
            result.setStatus(400);
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public HttpHeaders create()
    {
        DefaultHttpHeaders result = new DefaultHttpHeaders("show");

        if (model.hasGeneratedId())
        {
            E temp = getEmptyModel();
            model.obtainInstance().setId(temp.getId());
        }

        E temp = service.create(model.obtainInstance());

        if (service.hasPendingError())
        {
            error = new Error(service.getPendingErrorMessage());
            result.setStatus(400);
        } else if (temp == null)
        {
            error = new Error(DEFAULT_ERR_MSG);
            result.setStatus(400);
        } else
        {
            model = createProxy(temp);
            result.setLocationId(model.obtainInstance().getId());
        }

        return result;
    }

    public HttpHeaders destroy()
    {
        HttpHeaders result = new DefaultHttpHeaders("destroy");

        try
        {
            model = createProxy(service.find(convertId(id)));
            service.delete(model.obtainInstance());
        } catch (Exception e)
        {
            result.setStatus(400);
        }

        return result;
    }

    public HttpHeaders update()
    {
        E updatedModel = service.update(model.obtainInstance());

        DefaultHttpHeaders result = new DefaultHttpHeaders("show");

        if (service.hasPendingError())
        {
            error = new Error(service.getPendingErrorMessage());
            result.setStatus(400);
        } else if (updatedModel == null)
        {
            error = new Error(DEFAULT_ERR_MSG);
            result.setStatus(400);
        } else
        {
            model = createProxy(updatedModel);
            return result.setLocationId(updatedModel.getId());
        }

        return result;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public P getCurrentModel()
    {
        return this.model;
    }

    public void setCurrentModel(P model)
    {
        this.model = model;
    }

    @Override
    public Object getModel()
    {
        if (error != null)
        {
            return error;
        }

        if (list == null)
        {
            return model;
        }

        return list;
    }

    protected AbstractService<E> getService()
    {
        return this.service;
    }

    protected abstract AbstractService<E> createService();

    protected abstract Serializable convertId(String id);

    protected abstract E getEmptyModel();

    protected abstract P createProxy(E instance);

    protected abstract List<P> createProxy(List<E> instance);
}
