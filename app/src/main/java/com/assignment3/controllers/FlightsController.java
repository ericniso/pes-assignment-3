package com.assignment3.controllers;

import com.assignment3.entities.Flight;
import com.assignment3.proxies.FlightProxy;
import com.assignment3.services.AbstractService;
import com.assignment3.services.FlightService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightsController extends ControllerBase<Flight, FlightProxy>
{
    @Override
    protected AbstractService<Flight> createService()
    {
        return new FlightService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Flight getEmptyModel()
    {
        return new Flight();
    }

    @Override
    protected FlightProxy createProxy(Flight instance)
    {
        return new FlightProxy(instance);
    }

    @Override
    protected List<FlightProxy> createProxy(List<Flight> instance)
    {
        List<FlightProxy> list = new ArrayList<>();

        for (Flight i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
