package com.assignment3.controllers;

import com.assignment3.entities.Airport;
import com.assignment3.entities.Flight;
import com.assignment3.proxies.AirportProxy;
import com.assignment3.services.AirportService;
import com.assignment3.services.AbstractService;
import com.assignment3.services.FlightService;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AirportsController extends ControllerBase<Airport, AirportProxy>
{
    private FlightService flightService;
    private String flightId;

    @Override
    protected AbstractService<Airport> createService()
    {
        return new AirportService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Airport getEmptyModel()
    {
        return new Airport();
    }

    @Override
    protected AirportProxy createProxy(Airport instance)
    {
        return new AirportProxy(instance);
    }

    @Override
    protected List<AirportProxy> createProxy(List<Airport> instance)
    {
        List<AirportProxy> list = new ArrayList<>();

        for (Airport i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }

    public HttpHeaders arrivals()
    {
        DefaultHttpHeaders result = new DefaultHttpHeaders("show");
        try
        {
            FlightService flightService = new FlightService();
            String flightId = getCurrentModel().obtArrivalId();
            Airport currentModel = getService().find(getCurrentModel().getCode());
            setCurrentModel(createProxy(currentModel));
            Flight flight = flightService.find(flightId);
            flight.setArrival(getCurrentModel().obtainInstance());
            getCurrentModel().obtainInstance().addArrival(flight);
            getService().update(getCurrentModel().obtainInstance());
            flightService.update(flight);
        } catch (Exception ex)
        {
            result.setStatus(400);
        }

        return result;
    }

    public HttpHeaders departures()
    {
        DefaultHttpHeaders result = new DefaultHttpHeaders("show");
        try
        {
            FlightService flightService = new FlightService();
            String flightId = getCurrentModel().obtDepartureId();
            Airport currentModel = getService().find(getCurrentModel().getCode());
            setCurrentModel(createProxy(currentModel));
            Flight flight = flightService.find(flightId);
            flight.setDeparture(getCurrentModel().obtainInstance());
            getCurrentModel().obtainInstance().addDeparture(flight);
            getService().update(getCurrentModel().obtainInstance());
            flightService.update(flight);
        } catch (Exception ex)
        {
            result.setStatus(400);
        }

        return result;
    }

}
