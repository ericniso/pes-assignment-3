package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The entities are stored/retrieved in the RDBMS table using the value
 * {@code "pilot"} as a discriminator
 */
@Entity
@DiscriminatorValue("pilot")
public class Pilot extends Staff implements Serializable
{

    @Column(name = "grade")
    private String grade;

    @OneToMany(mappedBy = "captain", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Flight> captainOf;

    @OneToMany(mappedBy = "copilot", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Flight> copilotOf;

    public Pilot()
    {
        captainOf = new HashSet<>(0);
        copilotOf = new HashSet<>(0);
    }

    public Pilot(String name, String surname, Date birthday, String grade)
    {
        super(name, surname, birthday);
        setGrade(grade);
        captainOf = new HashSet<>(0);
        copilotOf = new HashSet<>(0);
    }

    public String getGrade()
    {
        return grade;
    }

    public void setGrade(String grade)
    {
        this.grade = grade;
    }

    public Set<Flight> getCaptainOf()
    {
        return captainOf;
    }

    public Set<Flight> getCopilotOf()
    {
        return copilotOf;
    }
}
