package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The entities are stored/retrieved in the RDBMS table using the value
 * {@code "assistant"} as a discriminator
 */
@Entity
@DiscriminatorValue("assistant")
public class Assistant extends Staff implements Serializable
{

    @Column(name = "language")
    private String language;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Flights_has_Staff",
        joinColumns = { @JoinColumn(name = "staff_id", referencedColumnName = "id") },
        inverseJoinColumns = { @JoinColumn(name = "flight_code", referencedColumnName = "code") })
    private Set<Flight> assistantOf;

    public Assistant()
    {
        assistantOf = new HashSet<>(0);
    }

    public Assistant(String name, String surname, Date birthday, String language)
    {
        super(name, surname, birthday);
        setLanguage(language);
        assistantOf = new HashSet<>(0);
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public Set<Flight> getAssistantOf()
    {
        return assistantOf;
    }

}
