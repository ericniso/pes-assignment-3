package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Airports")
public class Airport implements AbstractEntity<String>, Serializable
{

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state", nullable = false)
    private String state;

    @OneToMany(mappedBy = "departure", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Flight> departures;

    @OneToMany(mappedBy = "arrival", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Flight> arrivals;

    public Airport()
    {
        departures = new HashSet<>(0);
        arrivals = new HashSet<>(0);
    }

    public Airport(String code, String name, String city, String state)
    {
        setId(code);
        setName(name);
        setCity(city);
        setState(state);
        departures = new HashSet<>(0);
        arrivals = new HashSet<>(0);
    }

    @Override
    public String getId()
    {
        return code;
    }

    @Override
    public void setId(String id)
    {
        this.code = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public Set<Flight> getDepartures()
    {
        return departures;
    }

    public void addDeparture(Flight f)
    {
        departures.add(f);
    }

    public void addArrival(Flight f)
    {
        arrivals.add(f);
    }

    public Set<Flight> getArrivals()
    {
        return arrivals;
    }

    @Override
    public boolean hasGeneratedId()
    {
        return false;
    }
}
