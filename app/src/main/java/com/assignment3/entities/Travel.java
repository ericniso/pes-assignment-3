package com.assignment3.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Travels")
public class Travel implements AbstractEntity<String>, Serializable
{

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private String id;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TravelStatus status = TravelStatus.PLANNED;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private Company company;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Travels_has_Clients",
            joinColumns = {@JoinColumn(name = "travel_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "client_id", referencedColumnName = "id")})
    private Set<Client> clients;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Travels_related_Travels",
            joinColumns = {@JoinColumn(name = "travel_origin_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "travel_related_id", referencedColumnName = "id")})
    private Set<Travel> related;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "flight_code", referencedColumnName = "code")
    private Flight flight;

    public Travel()
    {
        clients = new HashSet<>(0);
        related = new HashSet<>(0);
    }

    public Travel(Date date, TravelStatus status)
    {
        setDate(date);
        setStatus(status);
        clients = new HashSet<>(0);
        related = new HashSet<>(0);
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public TravelStatus getStatus()
    {
        return status;
    }

    public void setStatus(TravelStatus status)
    {
        this.status = status;
    }

    public Company getCompany()
    {
        return company;
    }

    public void setCompany(Company company)
    {
        this.company = company;
    }

    public Set<Client> getClients()
    {
        return clients;
    }

    public Set<Travel> getRelated()
    {
        return related;
    }

    public Flight getFlight()
    {
        return flight;
    }

    public void setFlight(Flight flight)
    {
        this.flight = flight;
    }

    @Override
    public boolean hasGeneratedId()
    {
        return true;
    }

    public enum TravelStatus
    {
        PLANNED,
        IN_PROGRESS,
        CANCELLED,
        COMPLETED,
    }
}
