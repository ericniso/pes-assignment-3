package com.assignment3.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Airplanes")
public class Airplane implements AbstractEntity<String>, Serializable
{

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private String id;

    @Column(name = "manufacturer", nullable = false)
    private String manufacturer;

    @Column(name = "model", nullable = false)
    private String model;

    @OneToMany(mappedBy = "airplane", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Flight> flights;

    public Airplane()
    {
        flights = new HashSet<>(0);
    }

    public Airplane(String manufacturer, String model)
    {
        setManufacturer(manufacturer);
        setModel(model);
        flights = new HashSet<>(0);
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public Set<Flight> getFlights()
    {
        return flights;
    }

    public void addFlight(Flight f)
    {
        flights.add(f);
    }

    @Override
    public boolean hasGeneratedId()
    {
        return true;
    }
}
