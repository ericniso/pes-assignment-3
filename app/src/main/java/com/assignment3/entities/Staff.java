package com.assignment3.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Abstract class that represents the base superclass of {@link Pilot}
 * and {@link Assistant} entities. The inheritance strategy tells that the
 * specialized entities are stored in the same logical table inside the RDBMS
 * using the RDBMS table column {@code "staff_type"} as a discriminator
 */
@Entity
@Table(name = "Staff")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "staff_type", discriminatorType = DiscriminatorType.STRING)
public abstract class Staff implements AbstractEntity<String>
{

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "birthday", nullable = false)
    private Date birthday;

    public Staff()
    {

    }

    public Staff(String name, String surname, Date birthday)
    {
        setName(name);
        setSurname(surname);
        setBirthday(birthday);
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public Date getBirthday()
    {
        return birthday;
    }

    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    @Override
    public boolean hasGeneratedId()
    {
        return true;
    }
}
