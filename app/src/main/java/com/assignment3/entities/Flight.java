package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Flights")
public class Flight implements AbstractEntity<String>, Serializable
{

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "departure_time", nullable = false)
    private Date departureTime;

    @Column(name = "arrival_time", nullable = false)
    private Date arrivalTime;

    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Travel> travels;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_airport_code", referencedColumnName = "code")
    private Airport departure;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "arrival_airport_code", referencedColumnName = "code")
    private Airport arrival;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "airplane_id", referencedColumnName = "id")
    private Airplane airplane;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "captain_id", referencedColumnName = "id")
    private Pilot captain;

    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinColumn(name = "copilot_id", referencedColumnName = "id")
    private Pilot copilot;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Flights_has_Staff",
            joinColumns = { @JoinColumn(name = "flight_code", referencedColumnName = "code") },
            inverseJoinColumns = { @JoinColumn(name = "staff_id", referencedColumnName = "id") })
    private Set<Assistant> assistants;

    public Flight()
    {
        travels = new HashSet<>(0);
        assistants = new HashSet<>(0);
    }

    public Flight(String code, Date departureTime, Date arrivalTime)
    {
        setId(code);
        setDepartureTime(departureTime);
        setArrivalTime(arrivalTime);
        travels = new HashSet<>(0);
        assistants = new HashSet<>(0);
    }

    @Override
    public String getId()
    {
        return code;
    }

    @Override
    public void setId(String id)
    {
        this.code = id;
    }

    public Date getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime)
    {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public Airport getDeparture()
    {
        return departure;
    }

    public void setDeparture(Airport departure)
    {
        this.departure = departure;
    }

    public Airport getArrival()
    {
        return arrival;
    }

    public void setArrival(Airport arrival)
    {
        this.arrival = arrival;
    }

    public Set<Travel> getTravels()
    {
        return travels;
    }

    public Pilot getCaptain()
    {
        return captain;
    }

    public void setCaptain(Pilot captain)
    {
        this.captain = captain;
    }

    public Pilot getCopilot()
    {
        return copilot;
    }

    public void setCopilot(Pilot copilot)
    {
        this.copilot = copilot;
    }

    public Set<Assistant> getAssistants()
    {
        return assistants;
    }

    public Airplane getAirplane()
    {
        return airplane;
    }

    public void setAirplane(Airplane airplane)
    {
        this.airplane = airplane;
    }

    @Override
    public boolean hasGeneratedId()
    {
        return false;
    }
}
