package com.assignment3.entities;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Abstract class that represents the base superclass of {@link Client}
 * and {@link Company} entities. The inheritance strategy tells that the
 * specialized entities have their own logical representation inside the RDBMS
 * as separate tables.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class User implements AbstractEntity<String>
{

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id")
    private String id;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    public User()
    {

    }

    public User(String username, String password)
    {
        setUsername(username);
        setPassword(password);
    }

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    /**
     * Hashes the password in order to be stored in a slightly secure way.
     * @param password the password to be stored
     */
    public void setPassword(String password)
    {
        String hashed = DigestUtils
                .md5Hex(password).toUpperCase();

        this.password = hashed;
    }

    /**
     * Hashes the provided password and returns <code>true</code> if it matches
     * with the stored one, <code>false</code> otherwise.
     * @param check the password to compare
     * @return <code>true</code> the passwords match, <code>false</code>
     * otherwise
     */
    public boolean isPasswordMatch(String check)
    {
        String hashed = DigestUtils
                .md5Hex(check).toUpperCase();

        return getPassword().equals(hashed);
    }

    @Override
    public boolean hasGeneratedId()
    {
        return true;
    }
}
