package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.ClientDao;
import com.assignment3.entities.Client;

public class ClientService extends AbstractService<Client>
{
    @Override
    protected AbstractDao<Client> getDao()
    {
        return new ClientDao();
    }
}
