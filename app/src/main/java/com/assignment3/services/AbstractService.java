package com.assignment3.services;


import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.DomainException;
import com.assignment3.daos.InterDomainException;
import com.assignment3.entities.AbstractEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract class needed to wrap {@link AbstractDao} methods
 * @param <E> the {@link AbstractEntity} type
 */
public abstract class AbstractService<E extends AbstractEntity>
{
    private AbstractDao<E> dao;
    private boolean pendingError;
    private String pendingErrorMessage;

    protected AbstractService()
    {
        this.dao = getDao();
        setPendingError(false);
    }

    public E create(E instance)
    {
        try
        {
            E res = dao.create(instance);

            if (res == null)
            {
                setPendingError(true);
                setPendingErrorMessage("Error while creating resource");
            }

            return res;
        } catch (InterDomainException | DomainException e)
        {
            setPendingError(true);
            setPendingErrorMessage(e.getMessage());
            return null;
        }
    }

    public E find(Serializable id)
    {
        E res = dao.find(id);

        if (res == null)
        {
            setPendingError(true);
            setPendingErrorMessage("Resource not found");
        }

        return res;
    }

    public List<E> findAll()
    {
        return dao.findAll();
    }

    public E update(E instance)
    {
        try
        {
            return dao.update(instance);
        } catch (InterDomainException | DomainException e)
        {
            setPendingError(true);
            setPendingErrorMessage(e.getMessage());
            return null;
        }
    }

    public void delete(E instance)
    {
        dao.delete(instance);
    }

    protected abstract AbstractDao<E> getDao();

    public boolean hasPendingError()
    {
        return pendingError;
    }

    private void setPendingError(boolean pendingError)
    {
        this.pendingError = pendingError;
    }

    public String getPendingErrorMessage()
    {
        return pendingErrorMessage;
    }

    private void setPendingErrorMessage(String pendingErrorMessage)
    {
        this.pendingErrorMessage = pendingErrorMessage;
    }
}

