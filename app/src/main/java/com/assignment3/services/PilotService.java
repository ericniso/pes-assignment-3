package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.PilotDao;
import com.assignment3.entities.Pilot;

public class PilotService extends AbstractService<Pilot>
{

    @Override
    protected AbstractDao<Pilot> getDao()
    {
        return new PilotDao();
    }
}
