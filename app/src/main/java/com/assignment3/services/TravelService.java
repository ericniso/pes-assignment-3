package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.TravelDao;
import com.assignment3.entities.Travel;

public class TravelService extends AbstractService<Travel>
{

    @Override
    protected AbstractDao<Travel> getDao()
    {
        return new TravelDao();
    }
}
