package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.AssistantDao;
import com.assignment3.entities.Assistant;

public class AssistantService extends AbstractService<Assistant>
{
    @Override
    protected AbstractDao<Assistant> getDao()
    {
        return new AssistantDao();
    }
}
