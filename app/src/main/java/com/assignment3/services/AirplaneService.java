package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.AirplaneDao;
import com.assignment3.entities.Airplane;

public class AirplaneService extends AbstractService<Airplane>
{

    @Override
    protected AbstractDao<Airplane> getDao()
    {
        return new AirplaneDao();
    }
}
