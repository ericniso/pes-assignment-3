package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.FlightDao;
import com.assignment3.entities.Flight;

public class FlightService extends AbstractService<Flight>
{

    @Override
    protected AbstractDao<Flight> getDao()
    {
        return new FlightDao();
    }
}
