package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.AirportDao;
import com.assignment3.entities.Airport;

public class AirportService extends AbstractService<Airport>
{
    @Override
    protected AbstractDao<Airport> getDao()
    {
        return new AirportDao();
    }
}
