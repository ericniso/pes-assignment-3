package com.assignment3.proxies;

import com.assignment3.entities.Airport;
import com.assignment3.entities.Flight;
import com.assignment3.entities.Travel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FlightProxy extends AbstractProxy<Flight>
{
    public FlightProxy(Flight instance)
    {
        super(instance);
    }

    public String getCode()
    {
        return obtainInstance().getId();
    }

    public void setCode(String id)
    {
        obtainInstance().setId(id);
    }

    public String  getDepartureTime()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getDepartureTime());
    }

    public void setDepartureTime(String departureTime)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setDepartureTime(df.parse(departureTime));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getArrivalTime()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getArrivalTime());
    }

    public void setArrivalTime(String arrivalTime)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setArrivalTime(df.parse(arrivalTime));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getDeparture()
    {
        Airport airport = obtainInstance().getDeparture();
        return airport == null ? null : airport.getId();
    }

    public void setDeparture(Airport departure)
    {
        obtainInstance().setDeparture(departure);
    }

    public String getArrival()
    {
        Airport airport = obtainInstance().getArrival();
        return airport == null ? null : airport.getId();
    }

    public void setArrival(Airport arrival)
    {
        obtainInstance().setArrival(arrival);
    }

    public List<String> getTravels()
    {
        List<String> travels = new ArrayList<>();

        for (Travel t : obtainInstance().getTravels())
        {
            travels.add(t.getId());
        }

        return travels;
    }
}
