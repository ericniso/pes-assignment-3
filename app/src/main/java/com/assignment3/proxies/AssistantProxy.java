package com.assignment3.proxies;

import com.assignment3.entities.Assistant;
import com.assignment3.entities.Flight;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class AssistantProxy extends AbstractProxy<Assistant>
{
    public AssistantProxy(Assistant instance)
    {
        super(instance);
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public String getName()
    {
        return obtainInstance().getName();
    }

    public void setName(String name)
    {
        obtainInstance().setName(name);
    }

    public String getSurname()
    {
        return obtainInstance().getSurname();
    }

    public void setSurname(String surname)
    {
        obtainInstance().setSurname(surname);
    }

    public String getBirthday()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getBirthday());
    }

    public void setBirthday(String birthday)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setBirthday(df.parse(birthday));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public String getLanguage()
    {
        return obtainInstance().getLanguage();
    }

    public void setLanguage(String language)
    {
        obtainInstance().setLanguage(language);
    }

    public List<String> getAssistantOf()
    {
        List<String> flights = new ArrayList<>();

        for(Flight f : obtainInstance().getAssistantOf())
        {
            flights.add(f.getId());
        }

        return flights;
    }
}
