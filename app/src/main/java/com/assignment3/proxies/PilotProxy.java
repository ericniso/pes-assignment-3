package com.assignment3.proxies;

import com.assignment3.entities.Flight;
import com.assignment3.entities.Pilot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PilotProxy extends AbstractProxy<Pilot>
{

    public PilotProxy(Pilot instance)
    {
        super(instance);
    }

    public String getGrade()
    {
        return obtainInstance().getGrade();
    }

    public void setGrade(String grade)
    {
        obtainInstance().setGrade(grade);
    }

    public List<String> getCaptainOf()
    {
        List<String> flights = new ArrayList<>();

        for(Flight f: obtainInstance().getCaptainOf())
        {
            flights.add(f.getId());
        }

        return flights;
    }

    public List<String> getCopilotOf()
    {
        List<String> flights = new ArrayList<>();

        for(Flight f: obtainInstance().getCopilotOf())
        {
            flights.add(f.getId());
        }

        return flights;
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public String getName()
    {
        return obtainInstance().getName();
    }

    public void setName(String name)
    {
        obtainInstance().setName(name);
    }

    public String getSurname()
    {
        return obtainInstance().getSurname();
    }

    public void setSurname(String surname)
    {
        obtainInstance().setSurname(surname);
    }

    public String getBirthday()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getBirthday());
    }

    public void setBirthday(String birthday)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setBirthday(df.parse(birthday));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
