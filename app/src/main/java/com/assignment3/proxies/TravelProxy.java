package com.assignment3.proxies;

import com.assignment3.entities.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TravelProxy extends AbstractProxy<Travel>
{

    public TravelProxy(Travel instance)
    {
        super(instance);
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public String getDate()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getDate());
    }

    public void setDate(String date)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setDate(df.parse(date));
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public String getStatus()
    {
        return obtainInstance().getStatus().name();
    }

    public void setStatus(String status)
    {
        obtainInstance().setStatus(Travel.TravelStatus.valueOf(status));
    }

    public String getCompany()
    {
        Company company = obtainInstance().getCompany();
        return company == null ? null : company.getId();
    }

    public void setCompany(Company company)
    {
        obtainInstance().setCompany(company);
    }

    public List<String> getClients()
    {
        List<String> clients = new ArrayList<>();

        for (Client c : obtainInstance().getClients())
        {
            clients.add(c.getId());
        }

        return clients;
    }

    public List<String> getRelated()
    {
        List<String> travels = new ArrayList<>();

        for (Travel t : obtainInstance().getRelated())
        {
            travels.add(t.getId());
        }

        return travels;
    }

    public String getFlight()
    {
        Flight flight = obtainInstance().getFlight();
        return flight == null ? null : flight.getId();
    }

    public void setFlight(Flight flight)
    {
        obtainInstance().setFlight(flight);
    }
}
