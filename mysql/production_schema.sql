-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema skyscanner
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema skyscanner
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `skyscanner` DEFAULT CHARACTER SET utf8 ;
USE `skyscanner` ;

-- -----------------------------------------------------
-- Table `skyscanner`.`Clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Clients` (
  `id` VARCHAR(45) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `surname` VARCHAR(255) NOT NULL,
  `birthday` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Airports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Airports` (
  `code` VARCHAR(45) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `state` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Companies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Companies` (
  `id` VARCHAR(45) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `VAT` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Airplanes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Airplanes` (
  `id` VARCHAR(45) NOT NULL,
  `manufacturer` VARCHAR(255) NOT NULL,
  `model` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Staff` (
  `id` VARCHAR(45) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `surname` VARCHAR(255) NOT NULL,
  `birthday` DATE NOT NULL,
  `grade` VARCHAR(255) NULL,
  `language` VARCHAR(255) NULL,
  `staff_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Flights`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Flights` (
  `code` VARCHAR(45) NOT NULL,
  `departure_time` DATE NOT NULL,
  `arrival_time` DATE NOT NULL,
  `arrival_airport_code` VARCHAR(45) NULL,
  `departure_airport_code` VARCHAR(45) NULL,
  `airplane_id` VARCHAR(45) NULL,
  `captain_id` VARCHAR(45) NULL,
  `copilot_id` VARCHAR(45) NULL,
  PRIMARY KEY (`code`),
  INDEX `fk_Flights_Airports_idx` (`arrival_airport_code` ASC),
  INDEX `fk_Flights_Airports1_idx` (`departure_airport_code` ASC),
  INDEX `fk_Flights_Airplanes1_idx` (`airplane_id` ASC),
  INDEX `fk_Flights_Staff1_idx` (`captain_id` ASC),
  INDEX `fk_Flights_Staff2_idx` (`copilot_id` ASC),
  CONSTRAINT `fk_Flights_Airports`
    FOREIGN KEY (`arrival_airport_code`)
    REFERENCES `skyscanner`.`Airports` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flights_Airports1`
    FOREIGN KEY (`departure_airport_code`)
    REFERENCES `skyscanner`.`Airports` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flights_Airplanes1`
    FOREIGN KEY (`airplane_id`)
    REFERENCES `skyscanner`.`Airplanes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flights_Staff1`
    FOREIGN KEY (`captain_id`)
    REFERENCES `skyscanner`.`Staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flights_Staff2`
    FOREIGN KEY (`copilot_id`)
    REFERENCES `skyscanner`.`Staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Travels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Travels` (
  `id` VARCHAR(45) NOT NULL,
  `date` DATE NOT NULL,
  `status` VARCHAR(255) NOT NULL,
  `flight_code` VARCHAR(45) NULL,
  `company_id` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Travel_Flights1_idx` (`flight_code` ASC),
  INDEX `fk_Travel_Companies1_idx` (`company_id` ASC),
  CONSTRAINT `fk_Travel_Flights1`
    FOREIGN KEY (`flight_code`)
    REFERENCES `skyscanner`.`Flights` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Travel_Companies1`
    FOREIGN KEY (`company_id`)
    REFERENCES `skyscanner`.`Companies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Travels_related_Travels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Travels_related_Travels` (
  `travel_origin_id` VARCHAR(45) NOT NULL,
  `travel_related_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`travel_origin_id`, `travel_related_id`),
  INDEX `fk_Travel_has_Travel_Travel2_idx` (`travel_related_id` ASC),
  CONSTRAINT `fk_Travel_has_Travel_Travel1`
    FOREIGN KEY (`travel_origin_id`)
    REFERENCES `skyscanner`.`Travels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Travel_has_Travel_Travel2`
    FOREIGN KEY (`travel_related_id`)
    REFERENCES `skyscanner`.`Travels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Travels_has_Clients`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Travels_has_Clients` (
  `travel_id` VARCHAR(45) NOT NULL,
  `client_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`travel_id`, `client_id`),
  INDEX `fk_Travel_has_Clients_Clients1_idx` (`client_id` ASC),
  CONSTRAINT `fk_Travel_has_Clients_Travel1`
    FOREIGN KEY (`travel_id`)
    REFERENCES `skyscanner`.`Travels` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Travel_has_Clients_Clients1`
    FOREIGN KEY (`client_id`)
    REFERENCES `skyscanner`.`Clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `skyscanner`.`Flights_has_Staff`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `skyscanner`.`Flights_has_Staff` (
  `flight_code` VARCHAR(45) NOT NULL,
  `staff_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`flight_code`, `staff_id`),
  INDEX `fk_Flights_has_Staff_Staff1_idx` (`staff_id` ASC),
  CONSTRAINT `fk_Flights_has_Staff_Flights1`
    FOREIGN KEY (`flight_code`)
    REFERENCES `skyscanner`.`Flights` (`code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flights_has_Staff_Staff1`
    FOREIGN KEY (`staff_id`)
    REFERENCES `skyscanner`.`Staff` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
