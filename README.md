# PeS Assignment 3

Membri del gruppo: **Lorenzo Mammana #807391**, **Eric Nisoli #807147**

## Sotware requirements

* Linux distribution or Windows with git-bash shell, since `./test` and 
    `./deploy` scripts are written with bash commands
* docker
* docker-compose

Built and tested on:

* Ubuntu 16.04 / Windows 10 with git-bash shell
* docker 17.05.0-ce
* docker-compose 1.12.0

## Installation

In order to download and install the software type the following instructions 
inside a shell

```
$ git clone git@gitlab.com:EricNisoli/pes-assignment-3.git
```

## Execution

Before executing `test` and `deploy` commands, an environment variable named 
`ROOT` must be set and containing an alphanumerical string which serves as the 
root password for the mysql databases used by the software, an example is 
provided in the run instructions. It's also possible to specify a variable 
`SQ_DEBUG` in order to log all queries executed by Hibernate. `SQL_DEBUG` is set
to default `true` for `Test cases` and to default `false` for `Run`.


### Test cases

In order to execute JUnit test cases type the following instructions

```
$ cd /path/to/pes-assignment-3

$ ROOT=skyscanner ./test
```

or with `SQL_DEBUG`

```
$ cd /path/to/pes-assignment-3

$ ROOT=skyscanner SQL_DEBUG=false ./test
```

Every argument written after `./test` will be passed to the `mvn` command so 
that you can exploit all functionalities of Maven testing framework, such as 
testing only single classes:

```
$ cd /path/to/pes-assignment-3

$ ROOT=skyscanner ./test -Dtest=AssistantDaoTestCase
```

The results of test run will be printed on the console

### Software run

In order to run the software type the following instructions

```
$ cd /path/to/pes-assignment-3

$ ./deploy build

$ ROOT=skyscanner ./deploy up
```

or with `SQL_DEBUG`

```
$ cd /path/to/pes-assignment-3

$ ./deploy build

$ ROOT=skyscanner SQL_DEBUG=true ./deploy up
```

Now the software should be up and running in foreground, if you want to run 
it in the background, type the following

```
$ cd /path/to/pes-assignment-3

$ ./deploy build

$ ROOT=skyscanner ./deploy up -d
```

To shut it down just type

```
$ cd /path/to/pes-assignment-3

$ ./deploy down
```
